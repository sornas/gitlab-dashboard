use std::{collections::BTreeMap, path::Path};

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::{
    api::{issue_activity, mr_activity},
    util::hash_u64,
};

#[derive(Deserialize, Serialize)]
pub struct Storage {
    repositories: BTreeMap<String, (Vec<IssueActivity>, Vec<MrActivity>)>,
    last_checked: DateTime<Utc>,
}

impl Default for Storage {
    fn default() -> Self {
        Self {
            repositories: BTreeMap::default(),
            last_checked: Utc::now(),
        }
    }
}

impl Storage {
    pub fn open(path: impl AsRef<Path>) -> Option<Self> {
        serde_json::from_str(&std::fs::read_to_string(path).ok()?).ok()?
    }

    pub fn write(&self, path: impl AsRef<Path>) -> Result<(), ()> {
        std::fs::write(path, serde_json::to_string_pretty(self).map_err(|_| ())?)
            .map_err(|_| ())?;
        Ok(())
    }

    pub fn issues(&self) -> Vec<&IssueActivity> {
        self.repositories
            .values()
            .flat_map(|(issue, _mr)| issue)
            .collect()
    }

    pub fn mrs(&self) -> Vec<&MrActivity> {
        self.repositories
            .values()
            .flat_map(|(_issue, mr)| mr)
            .collect()
    }

    pub fn add_issues(&mut self, issues: &[issue_activity::response::Issue], since: DateTime<Utc>) {
        for issue in issues {
            let repository = self
                .repositories
                .entry(issue.repository.clone())
                .or_default();
            repository.0.extend(issue.notes.iter().flat_map(|note| {
                use issue_activity::response::NoteKind;
                let kind = match &note.kind {
                    NoteKind::Comment { body } => IssueActivityKind::Comment {
                        author: note.author.clone(),
                        body: body.clone(),
                    },
                    // Ignored activity:
                    NoteKind::UpdateDescription => return None,
                    // TODO: warning?
                    NoteKind::UnknownSystemNote { action: _, body: _ } => return None,
                };
                Some(IssueActivity {
                    repository: issue.repository.clone(),
                    id: issue.id.clone(),
                    title: issue.title.clone(),
                    date: note.created_at,
                    url: note.url.clone(),
                    kind,
                })
            }));
            if issue.created_at >= since {
                repository.0.push(IssueActivity {
                    repository: issue.repository.clone(),
                    id: issue.id.clone(),
                    title: issue.title.clone(),
                    date: issue.created_at,
                    url: issue.url.clone(),
                    kind: IssueActivityKind::Created {
                        author: issue.author.clone(),
                        body: issue.body.clone(),
                    },
                })
            }
            // sort reversed
            repository.0.sort_by(|a, b| b.cmp(a));
            repository.0.dedup();
        }
    }

    pub fn add_mrs(&mut self, mrs: &[mr_activity::response::Mr], since: DateTime<Utc>) {
        for mr in mrs {
            let repository = self.repositories.entry(mr.repository.clone()).or_default();
            repository.1.extend(mr.notes.iter().flat_map(|note| {
                use mr_activity::response::NoteKind;
                let kind = match &note.kind {
                    NoteKind::Comment { body } => MrActivityKind::Comment {
                        author: note.author.clone(),
                        body: body.clone(),
                    },
                    // Ignored activity:
                    NoteKind::UpdateDescription => return None,
                    // TODO: warning?
                    NoteKind::UnknownSystemNote { action: _, body: _ } => return None,
                };
                Some(MrActivity {
                    repository: mr.repository.clone(),
                    id: mr.id.clone(),
                    title: mr.title.clone(),
                    date: note.created_at,
                    url: note.url.clone(),
                    kind,
                })
            }));
            if mr.created_at >= since {
                repository.1.push(MrActivity {
                    repository: mr.repository.clone(),
                    id: mr.id.clone(),
                    title: mr.title.clone(),
                    date: mr.created_at,
                    url: mr.url.clone(),
                    kind: MrActivityKind::Created {
                        author: mr.author.clone(),
                        body: mr.body.clone(),
                    },
                })
            }
            if let Some(merged_at) = mr.merged_at {
                if merged_at >= since {
                    repository.1.push(MrActivity {
                        repository: mr.repository.clone(),
                        id: mr.id.clone(),
                        title: mr.title.clone(),
                        date: merged_at,
                        url: mr.url.clone(),
                        kind: MrActivityKind::Merged {
                            author: mr.merged_by.clone(),
                            target_branch: mr.target_branch.clone(),
                        },
                    })
                }
            }
            // sort reversed
            repository.1.sort_by(|a, b| b.cmp(a));
            repository.1.dedup();
        }
    }

    pub fn remove_activity(&mut self, hash: u64) {
        for repository in self.repositories.values_mut() {
            repository.0.retain(|activity| hash_u64(activity) != hash);
            repository.1.retain(|activity| hash_u64(activity) != hash);
        }
    }
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct IssueActivity {
    pub repository: String,
    pub id: String,
    pub title: String,
    pub date: DateTime<Utc>,
    pub url: String,
    pub kind: IssueActivityKind,
}

impl PartialOrd for IssueActivity {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for IssueActivity {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.date.cmp(&other.date)
    }
}

#[derive(Deserialize, Serialize, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum IssueActivityKind {
    Comment { author: String, body: String },
    Created { author: String, body: String },
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct MrActivity {
    pub repository: String,
    pub id: String,
    pub title: String,
    pub date: DateTime<Utc>,
    pub url: Option<String>,
    pub kind: MrActivityKind,
}

impl PartialOrd for MrActivity {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for MrActivity {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.date.cmp(&other.date)
    }
}

#[derive(Deserialize, Serialize, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum MrActivityKind {
    Comment {
        author: String,
        body: String,
    },
    Created {
        author: Option<String>,
        body: String,
    },
    Merged {
        author: Option<String>,
        target_branch: String,
    },
}
