use color_eyre::Result;
use eframe::egui;

use crate::ui::App;

mod api;
mod model;
mod ui;
mod util;

fn main() -> Result<()> {
    color_eyre::install().unwrap();

    let access_token = std::fs::read_to_string("access-token")?;
    let access_token = access_token.trim().to_string();

    eframe::run_native(
        "gitlab native ui",
        eframe::NativeOptions {
            viewport: egui::ViewportBuilder::default(),
            ..Default::default()
        },
        Box::new(|_cc| Box::new(App::new(access_token).unwrap())),
    )
    .unwrap();

    Ok(())
}
