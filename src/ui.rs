use chrono::{DateTime, Local, NaiveDate, NaiveDateTime, NaiveTime, Utc};
use eframe::egui;
use egui::collapsing_header::CollapsingState;
use flume::{Receiver, Sender};
use serde::Deserialize;

use crate::api::{issue_activity, mr_activity};
use crate::model::{self, IssueActivity, MrActivity};
use crate::util::hash_u64;

macro_rules! try_or_send {
    ($tx:expr, $expr:expr, $error:expr $( , return $return:expr )?) => {
        match $expr {
            Ok(x) => x,
            Err(e) => {
                #[allow(clippy::redundant_closure_call)]
                $tx.send(Message::Error($error(e))).unwrap();
                return $($return)?;
            }
        }
    };
}

#[derive(Deserialize, Default)]
struct Config {}

#[derive(Debug)]
pub enum ConfigurationError {
    NoHomeDirectory,
    NoConfigDirectory,
    NoConfigFile,
    Io(std::io::Error),
    Toml(toml::de::Error),
}

// TODO: enum View for different active views.
pub struct App {
    /// Current entries being considered for showing, before filtering
    entries: Vec<Entry>,
    /// Currently applied filter.
    filter: Filter,

    date_pick: NaiveDate,

    /// Issues with activities we know about
    storage: model::Storage,

    access_token: String,
    _config: Config,
    msg_tx: Sender<Message>,
    msg_rx: Receiver<Message>,

    statusbar_spinner: Option<String>,
}

pub enum Message {
    // TODO spinner context. right now the sequence [start, start, end] will stop the spinner
    Spinner(Option<String>),
    ActivityResponse(
        Vec<issue_activity::response::Issue>,
        Vec<mr_activity::response::Mr>,
        DateTime<Utc>,
    ),
    Error(String),
}

impl App {
    pub fn new(access_token: String) -> Result<Self, ConfigurationError> {
        let config = match Self::try_read_configuration() {
            // valid configuration
            Ok(c) => c,
            // configuration does not exist: ignore on initial load
            Err(ConfigurationError::NoConfigDirectory | ConfigurationError::NoConfigFile) => {
                Config::default()
            }
            Err(e) => return Err(e),
        };

        let storage = model::Storage::open("storage.json").unwrap_or_default();
        let (tx, rx) = flume::unbounded();
        let mut app = Self {
            entries: Vec::new(),
            filter: Filter::empty(),
            storage,
            date_pick: Local::now().date_naive(),
            access_token,
            _config: config,
            msg_tx: tx,
            msg_rx: rx,
            statusbar_spinner: None,
        };
        app.update_entries();

        Ok(app)
    }

    fn try_read_configuration() -> Result<Config, ConfigurationError> {
        let project_dir = directories::ProjectDirs::from("", "", "gitlab-dashboard")
            .ok_or(ConfigurationError::NoHomeDirectory)?;
        let config_dir = project_dir.config_dir();
        if !config_dir.exists() {
            return Err(ConfigurationError::NoConfigDirectory);
        }
        let config_file = config_dir.join("config.toml");
        if !config_file.exists() {
            return Err(ConfigurationError::NoConfigFile);
        }
        let config = std::fs::read_to_string(config_file).map_err(ConfigurationError::Io)?;
        toml::from_str(&config).map_err(ConfigurationError::Toml)
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::TopBottomPanel::top("views").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.button("Issue activity (spade-lang/spade)").clicked();
                ui.add(egui_extras::DatePickerButton::new(&mut self.date_pick));
                if ui.button("update").clicked() {
                    // TODO: how to handle multiple updates
                    // - signal any active thread to cancel?
                    // - disallow request until current has finished?
                    // - queue updates?
                    let access_token = self.access_token.clone();
                    let dt = NaiveDateTime::new(
                        self.date_pick,
                        NaiveTime::from_hms_opt(0, 0, 0).unwrap(),
                    )
                    .and_utc();
                    let msg_tx = self.msg_tx.clone();
                    msg_tx
                        .send(Message::Spinner(Some("issue activity".to_string())))
                        .unwrap();
                    std::thread::spawn(move || {
                        let issue_activity = try_or_send!(
                            msg_tx,
                            issue_activity::Query::get(&access_token, dt),
                            |e| format!("error updating issue activity: {e}")
                        );
                        let mr_activity = try_or_send!(
                            msg_tx,
                            mr_activity::Query::get(&access_token, dt),
                            |e| format!("error updating mr activity: {e}")
                        );
                        msg_tx
                            .send(Message::ActivityResponse(issue_activity, mr_activity, dt))
                            .unwrap();
                        msg_tx.send(Message::Spinner(None)).unwrap();
                    });
                }
            })
        });
        egui::TopBottomPanel::bottom("statusbar").show(ctx, |ui| {
            ui.horizontal(|ui| match &self.statusbar_spinner {
                Some(updating) => {
                    ui.spinner();
                    ui.label(format!("updating {}...", updating.as_str()));
                }
                None => {}
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical(|ui| {
                for entry in &mut self.entries {
                    if self.filter.allows(entry) {
                        entry.ui(ui);
                    }
                }
            });
        });

        // post-draw

        // remove entries that have been clicked on
        for entry in self.entries.iter().filter(|entry| entry.remove) {
            self.storage.remove_activity(entry.storage_hash);
        }
        self.storage.write("storage.json").unwrap();
        self.entries.retain(|e| !e.remove);

        let messages: Vec<_> = self.msg_rx.try_iter().collect();
        for msg in messages {
            self.handle_message(msg);
        }
    }
}

impl App {
    fn handle_message(&mut self, message: Message) {
        match message {
            Message::Spinner(spinner) => self.statusbar_spinner = spinner,
            Message::ActivityResponse(issues, mrs, since) => {
                self.storage.add_issues(&issues, since);
                self.storage.add_mrs(&mrs, since);
                self.storage.write("storage.json").unwrap();
                self.update_entries();
            }
            Message::Error(e) => todo!("{e}"),
        }
    }

    fn update_entries(&mut self) {
        let issues = self
            .storage
            .issues()
            .into_iter()
            .map(|activity| Entry::from_issue_activity(activity, self.msg_tx.clone()));
        let mrs = self
            .storage
            .mrs()
            .into_iter()
            .map(|activity| Entry::from_mr_activity(activity, self.msg_tx.clone()));
        self.entries = issues.chain(mrs).collect();
        self.entries.sort_by(|e1, e2| e2.date.cmp(&e1.date));
    }
}

struct Entry {
    id: u64,
    date: DateTime<Utc>,
    title: String,
    body: Option<String>,
    url: Option<String>,

    msg_tx: Sender<Message>,
    storage_hash: u64,
    remove: bool,
}

impl Entry {
    fn from_issue_activity(issue: &IssueActivity, msg_tx: Sender<Message>) -> Self {
        use crate::model::IssueActivityKind;
        let IssueActivity {
            repository: _,
            id,
            title: issue_title,
            date,
            url,
            kind,
        } = issue;
        let storage_hash = hash_u64(issue);
        match kind {
            IssueActivityKind::Comment { author, body } => Entry {
                id: storage_hash,
                date: *date,
                title: format!("{} wrote a comment on #{} ({})", author, id, issue_title),
                body: Some(body.clone()),
                url: Some(url.clone()),
                msg_tx,
                storage_hash,
                remove: false,
            },
            IssueActivityKind::Created { author, body } => Entry {
                id: storage_hash,
                date: *date,
                title: format!("{} created issue #{}: {}", author, id, issue_title),
                body: Some(body.clone()),
                url: Some(url.clone()),
                msg_tx,
                storage_hash,
                remove: false,
            },
        }
    }

    fn from_mr_activity(mr: &MrActivity, msg_tx: Sender<Message>) -> Self {
        use crate::model::MrActivityKind;
        let MrActivity {
            repository: _,
            id,
            title,
            date,
            url,
            kind,
        } = mr;
        let storage_hash = hash_u64(mr);
        match kind {
            MrActivityKind::Comment { author, body } => Entry {
                id: storage_hash,
                date: *date,
                title: format!("{} wrote a comment on !{} ({})", author, id, title),
                body: Some(body.clone()),
                url: url.clone(),
                msg_tx,
                storage_hash,
                remove: false,
            },
            MrActivityKind::Created { author, body } => Entry {
                id: storage_hash,
                date: *date,
                title: format!(
                    "{} created MR !{}: {}",
                    author.as_deref().unwrap_or("<unknown user>"),
                    id,
                    title
                ),
                body: Some(body.clone()),
                url: url.clone(),
                msg_tx,
                storage_hash,
                remove: false,
            },
            MrActivityKind::Merged {
                author,
                target_branch,
            } => Entry {
                id: storage_hash,
                date: *date,
                title: format!(
                    "{} merged MR !{} into {}",
                    author.as_deref().unwrap_or("<unknown user>"),
                    id,
                    target_branch
                ),
                body: None,
                url: url.clone(),
                msg_tx,
                storage_hash,
                remove: false,
            },
        }
    }

    fn ui(&mut self, ui: &mut egui::Ui) {
        let id = ui.make_persistent_id(self.id);
        let mut header = |ui: &mut egui::Ui| {
            if ui.button("✔").clicked() {
                self.remove = true;
            }
            if let Some(url) = &self.url {
                if ui.button("⤴").clicked() {
                    try_or_send!(self.msg_tx, opener::open_browser(url), |e| format!(
                        "couldn't open '{}' in the browser: {}",
                        url, e
                    ));
                }
            }
            ui.label(&self.title);
        };

        // TODO: collapsing state arrow more to the right?
        if let Some(body) = &self.body {
            CollapsingState::load_with_default_open(ui.ctx(), id, false)
                .show_header(ui, header)
                .body(|ui| ui.label(body));
        } else {
            ui.horizontal(header);
        }
    }
}

struct Filter {}

impl Filter {
    fn empty() -> Self {
        Self {}
    }

    fn allows(&self, _entry: &Entry) -> bool {
        true
    }
}
