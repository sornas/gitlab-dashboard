use std::hash::{DefaultHasher, Hash, Hasher};

pub fn hash_u64<T: Hash>(t: &T) -> u64 {
    let mut hasher = DefaultHasher::new();
    t.hash(&mut hasher);
    hasher.finish()
}
