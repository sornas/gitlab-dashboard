pub mod issue_activity {
    use std::str::FromStr as _;

    use chrono::{DateTime, ParseError, Utc};
    use graphql_client::{GraphQLQuery, Response};

    type Time = String;
    pub type QueryResponse = Result<response::Response, Error>;

    #[derive(GraphQLQuery)]
    #[graphql(
        schema_path = "graphql/gitlab-schema-20240308.json",
        query_path = "graphql/gitlab-issue-activity.graphql",
        response_derives = "Debug"
    )]
    pub struct Query;

    pub mod response {
        use super::*;

        pub type Response = Vec<Issue>;

        #[derive(Debug, Hash)]
        pub struct Issue {
            pub repository: String,
            pub title: String,
            pub body: String,
            pub author: String,
            pub id: String,
            pub url: String,
            pub created_at: DateTime<Utc>,
            pub updated_at: DateTime<Utc>,
            pub closed_at: Option<DateTime<Utc>>,
            pub notes: Vec<Note>,
        }

        #[derive(Debug, Hash)]
        pub struct Note {
            pub created_at: DateTime<Utc>,
            pub author: String,
            pub kind: NoteKind,
            pub url: String,
        }

        #[derive(Debug, Hash)]
        pub enum NoteKind {
            Comment { body: String },
            UpdateDescription,
            UnknownSystemNote { action: String, body: String },
        }

        #[derive(Debug, Hash)]
        pub struct SystemNote {}
    }

    #[derive(Debug)]
    pub enum Error {
        /// Something is missing in the API that is required.
        ///
        /// The string contains information about what field was expected.
        MissingInResponse(&'static str),
        /// Something went wrong when parsing a DateTime object.
        Chrono(ParseError),
        /// Something went wrong when making the HTTP request.
        Http(reqwest::Error),
        /// Something went wrong when parsing the response as JSON.
        Json(serde_json::Error),
    }

    impl std::fmt::Display for Error {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Error::MissingInResponse(what) => write!(
                    f,
                    "expected field '{what}' in the response, but it wasn't there"
                ),
                Error::Chrono(e) => write!(f, "chrono parse error: {e}"),
                Error::Http(e) => write!(f, "http error: {e}"),
                Error::Json(e) => write!(f, "json parse error: {e}"),
            }
        }
    }

    impl Query {
        pub fn get(user_access_token: &str, since: DateTime<Utc>) -> QueryResponse {
            let query = Self::build_query(query::Variables {
                since: since.to_rfc3339(),
            });
            // TODO: pagination
            let resp = reqwest::blocking::Client::new()
                .post("https://gitlab.com/api/graphql")
                .header("Authorization", format!("Bearer {user_access_token}"))
                .json(&query)
                .send()
                .map_err(Error::Http)?
                .text()
                .map_err(Error::Http)?;
            let data: Response<query::ResponseData> =
                serde_json::from_str(&resp).map_err(Error::Json)?;

            data.data
                .ok_or(Error::MissingInResponse("data"))?
                .project
                .ok_or(Error::MissingInResponse("data.project"))?
                .issues
                .ok_or(Error::MissingInResponse("data.project.issues"))?
                .nodes
                .ok_or(Error::MissingInResponse("data.project.issues.nodes"))?
                .into_iter()
                .flatten() // flatten to filter out None-issues
                .map(|issue| {
                    let notes = issue
                        .notes
                        .nodes
                        .as_deref()
                        .unwrap_or(&[])
                        .iter()
                        .flatten()
                        .map(|note| {
                            let system_action = note
                                .system_note_metadata
                                .as_ref()
                                .and_then(|meta| meta.action.as_deref());
                            let kind = if let Some(system_action) = system_action {
                                match system_action {
                                    "description" => response::NoteKind::UpdateDescription,
                                    action => response::NoteKind::UnknownSystemNote {
                                        action: action.to_string(),
                                        body: note.body.to_string(),
                                    },
                                }
                            } else {
                                response::NoteKind::Comment {
                                    body: note.body.to_string(),
                                }
                            };
                            let url = match &note.url {
                                Some(url) => url.to_string(),
                                None => issue.web_url.to_string(),
                            };
                            let note = response::Note {
                                created_at: note.created_at.parse().map_err(Error::Chrono)?,
                                author: note
                                    .author
                                    .as_ref()
                                    .ok_or(Error::MissingInResponse("note.author"))?
                                    .username
                                    .to_string(),
                                kind,
                                url,
                            };
                            Ok(note)
                        })
                        .collect::<Result<Vec<_>, _>>()?;
                    let issue = response::Issue {
                        // TODO:
                        repository: "spade-lang/spade".to_string(),
                        title: issue.title,
                        body: issue.description.unwrap_or_default(),
                        author: issue.author.username.to_string(),
                        id: issue.iid,
                        url: issue.web_url,
                        created_at: issue.created_at.parse().map_err(Error::Chrono)?,
                        updated_at: issue.updated_at.parse().map_err(Error::Chrono)?,
                        closed_at: issue
                            .closed_at
                            .as_deref()
                            .map(chrono::DateTime::from_str)
                            .transpose()
                            .map_err(Error::Chrono)?,
                        notes,
                    };
                    Ok(issue)
                })
                .collect::<Result<Vec<_>, _>>()
        }
    }
}

pub mod mr_activity {
    use std::str::FromStr as _;

    use chrono::{DateTime, ParseError, Utc};
    use graphql_client::{GraphQLQuery, Response};

    type Time = String;
    pub type QueryResponse = Result<response::Response, Error>;

    #[derive(GraphQLQuery)]
    #[graphql(
        schema_path = "graphql/gitlab-schema-20240308.json",
        query_path = "graphql/gitlab-mr-activity.graphql",
        response_derives = "Debug"
    )]
    pub struct Query;

    pub mod response {
        use super::*;

        pub type Response = Vec<Mr>;

        pub struct Mr {
            pub repository: String,
            pub title: String,
            pub body: String,
            pub author: Option<String>,
            pub id: String,
            pub url: Option<String>,
            pub created_at: DateTime<Utc>,
            pub updated_at: DateTime<Utc>,
            pub merged_at: Option<DateTime<Utc>>,
            pub merged_by: Option<String>,
            pub target_branch: String,
            pub notes: Vec<Note>,
        }

        #[derive(Debug, Hash)]
        pub struct Note {
            pub created_at: DateTime<Utc>,
            pub author: String,
            pub kind: NoteKind,
            pub url: Option<String>,
        }

        #[derive(Debug, Hash)]
        pub enum NoteKind {
            Comment { body: String },
            UpdateDescription,
            UnknownSystemNote { action: String, body: String },
        }

        #[derive(Debug, Hash)]
        pub struct SystemNote {}
    }

    #[derive(Debug)]
    pub enum Error {
        /// Something is missing in the API that is required.
        ///
        /// The string contains information about what field was expected.
        MissingInResponse(&'static str),
        /// Something went wrong when parsing a DateTime object.
        Chrono(ParseError),
        /// Something went wrong when making the HTTP request.
        Http(reqwest::Error),
        /// Something went wrong when parsing the response as JSON.
        Json(serde_json::Error),
    }

    impl std::fmt::Display for Error {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Error::MissingInResponse(what) => write!(
                    f,
                    "expected field '{what}' in the response, but it wasn't there"
                ),
                Error::Chrono(e) => write!(f, "chrono parse error: {e}"),
                Error::Http(e) => write!(f, "http error: {e}"),
                Error::Json(e) => write!(f, "json parse error: {e}"),
            }
        }
    }

    impl Query {
        pub fn get(user_access_token: &str, since: DateTime<Utc>) -> QueryResponse {
            let query = Self::build_query(query::Variables {
                since: since.to_rfc3339(),
            });
            let resp = reqwest::blocking::Client::new()
                .post("https://gitlab.com/api/graphql")
                .header("Authorization", format!("Bearer {user_access_token}"))
                .json(&query)
                .send()
                .map_err(Error::Http)?
                .text()
                .map_err(Error::Http)?;
            let data: Response<query::ResponseData> =
                serde_json::from_str(&resp).map_err(Error::Json)?;

            data.data
                .ok_or(Error::MissingInResponse("data"))?
                .project
                .ok_or(Error::MissingInResponse("data.project"))?
                .merge_requests
                .ok_or(Error::MissingInResponse("data.project.merge_requests"))?
                .nodes
                .ok_or(Error::MissingInResponse(
                    "data.project.merge_requests.nodes",
                ))?
                .into_iter()
                .flatten() // flatten to filter out None-MRs
                .map(|mr| {
                    let notes = mr
                        .notes
                        .nodes
                        .as_deref()
                        .unwrap_or(&[])
                        .iter()
                        .flatten()
                        .map(|note| {
                            let system_action = note
                                .system_note_metadata
                                .as_ref()
                                .and_then(|meta| meta.action.as_deref());
                            let kind = if let Some(system_action) = system_action {
                                match system_action {
                                    "description" => response::NoteKind::UpdateDescription,
                                    action => response::NoteKind::UnknownSystemNote {
                                        action: action.to_string(),
                                        body: note.body.to_string(),
                                    },
                                }
                            } else {
                                response::NoteKind::Comment {
                                    body: note.body.to_string(),
                                }
                            };
                            let url = match &note.url {
                                Some(url) => Some(url.to_string()),
                                None => mr.web_url.as_ref().map(|x| x.to_string()),
                            };
                            let note = response::Note {
                                created_at: note.created_at.parse().map_err(Error::Chrono)?,
                                author: note
                                    .author
                                    .as_ref()
                                    .ok_or(Error::MissingInResponse("note.author"))?
                                    .username
                                    .to_string(),
                                kind,
                                url,
                            };
                            Ok(note)
                        })
                        .collect::<Result<Vec<_>, _>>()?;
                    let mr = response::Mr {
                        // TODO:
                        repository: "spade-lang/spade".to_string(),
                        title: mr.title,
                        body: mr.description.unwrap_or_default(),
                        author: mr.author.map(|x| x.username.to_string()),
                        id: mr.iid,
                        url: mr.web_url,
                        created_at: mr.created_at.parse().map_err(Error::Chrono)?,
                        updated_at: mr.updated_at.parse().map_err(Error::Chrono)?,
                        merged_at: mr
                            .merged_at
                            .as_deref()
                            .map(chrono::DateTime::from_str)
                            .transpose()
                            .map_err(Error::Chrono)?,
                        merged_by: mr.merge_user.as_ref().map(|x| x.username.to_string()),
                        target_branch: mr.target_branch,
                        notes,
                    };
                    Ok(mr)
                })
                .collect::<Result<Vec<_>, _>>()
        }
    }
}
